## List of EDI Transactions
#X12

     **EDI Code**        **EDI Transaction ** 
   ---------------------------------------------
    EDI 100           Insurance Plan Description
    EDI 101           Name And Address Lists
    EDI 102           Associated Data
    EDI 103           Abandoned Property Filings
    EDI 104           Air Shipment Information
    EDI 105           Business Entity Filings
    EDI 106           Motor Carrier Rate Proposal
    EDI 107           Request For Motor Carrier Rate Proposal
    EDI 108           Response To A Motor Carrier Rate Proposal
    EDI 109           Vessel Content Details
    EDI 110           Air Freight Details And Invoice
    EDI 111           Individual Insurance Policy And Client Information
    EDI 112           Property Damage Report
    EDI 113           Election Campaign And Lobbyist Reporting
    EDI 120           Vehicle Shipping Order
    EDI 121           Vehicle Service
    EDI 124           Vehicle Damage
    EDI 125           Multilevel Railcar Load Details
    EDI 126           Vehicle Application Advice
    EDI 127           Vehicle Baying Order
    EDI 128           Dealer Information
    EDI 129           Vehicle Carrier Rate Update
    EDI 130           Student Educational Record (Transcript)
    EDI 131           Student Educational Record (Transcript) Acknowledgment
    EDI 132           Human Resource Information
    EDI 133           Educational Institution Record
    EDI 135           Student Aid Origination Record
    EDI 138           Educational Testing And Prospect Request And Reporting
    EDI 139           Student Loan Guarantee Result
    EDI 140           Product Registration
    EDI 141           Product Service Claim Response
    EDI 142           Product Service Claim
    EDI 143           Product Service Notification
    EDI 144           Student Loan Transfer And Status Verification
    EDI 146           Request For Student Educational Record (Transcript)
    EDI 147           Response To Request For Student Educational Record
    EDI 148           Report Of Injury, Illness Or Incident
    EDI 149           Notice Of Tax Adjustment Or Assessment
    EDI 150           Tax Rate Notification
    EDI 151           Electronic Filing Of Tax Return Data Acknowledgment
    EDI 152           Statistical Government Information
    EDI 153           Unemployment Insurance Tax Claim Or Charge Information
    EDI 154           Secured Interest Filing
    EDI 155           Business Credit Report
    EDI 157           Notice Of Power Of Attorney
    EDI 158           Tax Jurisdiction Sourcing
    EDI 159           Motion Picture Booking Confirmation
    EDI 160           Transportation Automatic Equipment Identification
    EDI 161           Train Sheet
    EDI 163           Transportation Appointment Schedule Information
    EDI 170           Revenue Receipts Statement
    EDI 175           Court And Law Enforcement Notice
    EDI 176           Court Submission
    EDI 179           Environmental Compliance Reporting
    EDI 180           Return Merchandise Authorization And Notification
    EDI 185           Royalty Regulatory Report
    EDI 186           Insurance Underwriting Requirements Reporting
    EDI 187           Premium Audit Request And Return
    EDI 188           Educational Course Inventory
    EDI 189           Application For Admission To Educational Institution
    EDI 190           Student Enrollment Verification
    EDI 191           Student Loan Pre-Claims And Claims
    EDI 194           Grant Or Assistance Application
    EDI 195           Federal Communications Commission (Fcc) License Application
    EDI 196           Contractor Cost Data Reporting
    EDI 197           Real Estate Title Evidence
    EDI 198           Loan Verification Information
    EDI 199           Real Estate Settlement Information
    EDI 200           Mortgage Credit Report
    EDI 201           Residential Loan Application
    EDI 202           Secondary Mortgage Market Loan Delivery
    EDI 203           Secondary Mortgage Market Investor Report
    EDI 204           Motor Carrier Load Tender
    EDI 205           Mortgage Note
    EDI 206           Real Estate Inspection
    EDI 210           Motor Carrier Freight Details And Invoice
    EDI 211           Motor Carrier Bill Of Lading
    EDI 212           Motor Carrier Delivery Trailer Manifest
    EDI 213           Motor Carrier Shipment Status Inquiry
    EDI 214           Transportation Carrier Shipment Status Message
    EDI 215           Motor Carrier Pickup Manifest
    EDI 216           Motor Carrier Shipment Pickup Notification
    EDI 217           Motor Carrier Loading And Route Guide
    EDI 218           Motor Carrier Tariff Information
    EDI 219           Logistics Service Request
    EDI 220           Logistics Service Response
    EDI 222           Cartage Work Assignment
    EDI 223           Consolidators Freight Bill And Invoice
    EDI 224           Motor Carrier Summary Freight Bill Manifest
    EDI 225           Response To A Cartage Work Assignment
    EDI 227           Trailer Usage Report
    EDI 228           Equipment Inspection Report
    EDI 240           Motor Carrier Package Status
    EDI 242           Data Status Tracking
    EDI 244           Product Source Information
    EDI 245           Real Estate Tax Service Response
    EDI 248           Account Assignment/Inquiry And Service/Status
    EDI 249           Animal Toxicological Data
    EDI 250           Purchase Order Shipment Management Document
    EDI 251           Pricing Support
    EDI 252           Insurance Producer Administration
    EDI 255           Underwriting Information Services
    EDI 256           Periodic Compensation
    EDI 259           Residential Mortgage Insurance Explanation Of Benefits
    EDI 260           Application For Mortgage Insurance Benefits
    EDI 261           Real Estate Information Request
    EDI 262           Real Estate Information Report
    EDI 263           Residential Mortgage Insurance Application Response
    EDI 264           Mortgage Loan Default Status
    EDI 265           Real Estate Title Insurance Services Order
    EDI 266           Mortgage Or Property Record Change Notification
    EDI 267           Individual Life, Annuity And Disability Application
    EDI 268           Annuity Activity
    EDI 269           Health Care Benefit Coordination Verification
    EDI 270           Eligibility, Coverage Or Benefit Inquiry
    EDI 271           Eligibility, Coverage Or Benefit Information
    EDI 272           Property And Casualty Loss Notification
    EDI 273           Insurance/Annuity Application Status
    EDI 274           Healthcare Provider Information
    EDI 275           Patient Information
    EDI 276           Health Care Claim Status Request
    EDI 277           Health Care Information Status Notification
    EDI 278           Health Care Services Review Information
    EDI 280           Voter Registration Information
    EDI 283           Tax Or Fee Exemption Certification
    EDI 284           Commercial Vehicle Safety Reports
    EDI 285           Commercial Vehicle Safety And Credentials Information
    EDI 286           Commercial Vehicle Credentials
    EDI 288           Wage Determination
    EDI 290           Cooperative Advertising Agreements
    EDI 300           Reservation (Booking Request) (Ocean)
    EDI 301           Confirmation (Ocean)
    EDI 303           Booking Cancellation (Ocean)
    EDI 304           Shipping Instructions
    EDI 309           Customs Manifest
    EDI 310           Freight Receipt And Invoice (Ocean)
    EDI 311           Canada Customs Information
    EDI 312           Arrival Notice (Ocean)
    EDI 313           Shipment Status Inquiry (Ocean)
    EDI 315           Status Details (Ocean)
    EDI 317           Delivery/Pickup Order
    EDI 319           Terminal Information
    EDI 322           Terminal Operations And Intermodal Ramp Activity
    EDI 323           Vessel Schedule And Itinerary (Ocean)
    EDI 324           Vessel Stow Plan (Ocean)
    EDI 325           Consolidation Of Goods In Container
    EDI 326           Consignment Summary List
    EDI 350           Customs Status Information
    EDI 352           U.S. Customs Carrier General Order Status
    EDI 353           Customs Events Advisory Details
    EDI 354           U.S. Customs Automated Manifest Archive Status
    EDI 355           U.S. Customs Acceptance/Rejection
    EDI 356           U.S. Customs Permit To Transfer Request
    EDI 357           U.S. Customs In-Bond Information
    EDI 358           Customs Consist Information
    EDI 361           Carrier Interchange Agreement (Ocean)
    EDI 362           Cargo Insurance Advice Of Shipment
    EDI 404           Rail Carrier Shipment Information
    EDI 410           Rail Carrier Freight Details And Invoice
    EDI 412           Trailer Or Container Repair Billing
    EDI 414           Rail Carhire Settlements
    EDI 417           Rail Carrier Waybill Interchange
    EDI 418           Rail Advance Interchange Consist
    EDI 419           Advance Car Disposition
    EDI 420           Car Handling Information
    EDI 421           Estimated Time Of Arrival And Car Scheduling
    EDI 422           Equipment Order
    EDI 423           Rail Industrial Switch List
    EDI 424           Rail Carrier Services Settlement
    EDI 425           Rail Waybill Request
    EDI 426           Rail Revenue Waybill
    EDI 429           Railroad Retirement Activity
    EDI 431           Railroad Station Master File
    EDI 432           Rail Deprescription
    EDI 433           Railroad Reciprocal Switch File
    EDI 434           Railroad Mark Register Update Activity
    EDI 435           Standard Transportation Commodity Code Master
    EDI 436           Locomotive Information
    EDI 437           Railroad Junctions And Interchanges Activity
    EDI 440           Shipment Weights
    EDI 451           Railroad Event Report
    EDI 452           Railroad Problem Log Inquiry Or Advice
    EDI 453           Railroad Service Commitment Advice
    EDI 455           Railroad Parameter Trace Registration
    EDI 456           Railroad Equipment Inquiry Or Advice
    EDI 460           Railroad Price Distribution Request Or Response
    EDI 463           Rail Rate Reply
    EDI 466           Rate Request
    EDI 468           Rate Docket Journal Log
    EDI 470           Railroad Clearance
    EDI 475           Rail Route File Maintenance
    EDI 485           Ratemaking Action
    EDI 486           Rate Docket Expiration
    EDI 490           Rate Group Definition
    EDI 492           Miscellaneous Rates
    EDI 494           Rail Scale Rates
    EDI 500           Medical Event Reporting
    EDI 501           Vendor Performance Review
    EDI 503           Pricing History
    EDI 504           Clauses And Provisions
    EDI 511           Requisition
    EDI 517           Material Obligation Validation
    EDI 521           Income Or Asset Offset
    EDI 527           Material Due-In And Receipt
    EDI 536           Logistics Reassignment
    EDI 540           Notice Of Employment Status
    EDI 561           Contract Abstract
    EDI 567           Contract Completion Status
    EDI 568           Contract Payment Management Report
    EDI 601           U.S. Customs Export Shipment Information
    EDI 602           Transportation Services Tender
    EDI 620           Excavation Communication
    EDI 625           Well Information
    EDI 650           Maintenance Service Order
    EDI 715           Intermodal Group Loading Plan
    EDI 753           Request For Routing Instructions
    EDI 754           Routing Instructions
    EDI 805           Contract Pricing Proposal
    EDI 806           Project Schedule Reporting
    EDI 810           Invoice
    EDI 811           Consolidated Service Invoice/Statement
    EDI 812           Credit/Debit Adjustment
    EDI 813           Electronic Filing Of Tax Return Data
    EDI 814           General Request, Response Or Confirmation
    EDI 815           Cryptographic Service Message
    EDI 816           Organizational Relationships
    EDI 818           Commission Sales Report
    EDI 819           Joint Interest Billing And Operating Expense Statement
    EDI 820           Payment Order/Remittance Advice
    EDI 821           Financial Information Reporting
    EDI 822           Account Analysis
    EDI 823           Lockbox
    EDI 824           Application Advice
    EDI 826           Tax Information Exchange
    EDI 827           Financial Return Notice
    EDI 828           Debit Authorization
    EDI 829           Payment Cancellation Request
    EDI 830           Planning Schedule With Release Capability
    EDI 831           Application Control Totals
    EDI 832           Price/Sales Catalog
    EDI 833           Mortgage Credit Report Order
    EDI 834           Benefit Enrollment And Maintenance
    EDI 835           Health Care Claim Payment/Advice
    EDI 836           Procurement Notices
    EDI 837           Health Care Claim
    EDI 838           Trading Partner Profile
    EDI 839           Project Cost Reporting
    EDI 840           Request For Quotation
    EDI 841           Specifications/Technical Information
    EDI 842           Nonconformance Report
    EDI 843           Response To Request For Quotation
    EDI 844           Product Transfer Account Adjustment
    EDI 845           Price Authorization Acknowledgment/Status
    EDI 846           Inventory Inquiry/Advice
    EDI 847           Material Claim
    EDI 848           Material Safety Data Sheet
    EDI 849           Response To Product Transfer Account Adjustment
    EDI 850           Purchase Order
    EDI 851           Asset Schedule
    EDI 852           Product Activity Data
    EDI 853           Routing And Carrier Instruction
    EDI 854           Shipment Delivery Discrepancy Information
    EDI 855           Purchase Order Acknowledgment
    EDI 856           Advanced Ship Notice (ASN) /Manifest
    EDI 857           Shipment And Billing Notice
    EDI 858           Shipment Information
    EDI 859           Freight Invoice
    EDI 860           Purchase Order Change Request – Buyer Initiated
    EDI 861           Receiving Advice/Acceptance Certificate
    EDI 862           Shipping Schedule
    EDI 863           Report Of Test Results
    EDI 864           Text Message
    EDI 865           Purchase Order Change Acknowledgment/Request – Sell
    EDI 866           Production Sequence
    EDI 867           Product Transfer And Resale Report
    EDI 868           Electronic Form Structure
    EDI 869           Order Status Inquiry
    EDI 870           Order Status Report
    EDI 871           Component Parts Content
    EDI 872           Residential Mortgage Insurance Application
    EDI 873           Commodity Movement Services
    EDI 874           Commodity Movement Services Response
    EDI 875           Grocery Products Purchase Order
    EDI 876           Grocery Products Purchase Order Change
    EDI 877           Manufacturer Coupon Family Code Structure
    EDI 878           Product Authorization/De-Authorization
    EDI 879           Price Information
    EDI 880           Grocery Products Invoice
    EDI 881           Manufacturer Coupon Redemption Detail
    EDI 882           Direct Store Delivery Summary Information
    EDI 883           Market Development Fund Allocation
    EDI 884           Market Development Fund Settlement
    EDI 885           Retail Account Characteristics
    EDI 886           Customer Call Reporting
    EDI 887           Coupon Notification
    EDI 888           Item Maintenance
    EDI 889           Promotion Announcement
    EDI 891           Deduction Research Report
    EDI 893           Item Information Request
    EDI 894           Delivery/Return Base Record
    EDI 895           Delivery/Return Acknowledgment Or Adjustment
    EDI 896           Product Dimension Maintenance
    EDI 897           Data Synchronization
    EDI 920           Loss Or Damage Claim – General Commodities
    EDI 924           Loss Or Damage Claim – Motor Vehicle
    EDI 925           Claim Tracer
    EDI 926           Claim Status Report And Tracer Reply
    EDI 928           Automotive Inspection Detail
    EDI 940           Warehouse Shipping Order
    EDI 943           Warehouse Stock Transfer Shipment Advice
    EDI 944           Warehouse Stock Transfer Receipt Advice
    EDI 945           Warehouse Shipping Advice
    EDI 947           Warehouse Inventory Adjustment Advice
    EDI 980           Functional Group Totals
    EDI 990           Response To A Load Tender
    EDI 993           Secured Receipt Or Acknowledgment
    EDI 996           File Transfer
    EDI 997           Functional Acknowledgment
    EDI 998           Set Cancellation
    EDI 999           Implementation Acknowledgment
